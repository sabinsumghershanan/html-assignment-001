$(function () {
	var categories;
	var tasks;
	var selectedCategoryId;


	//Populates Category list
    $.get("../resources/data/categories.json", function(data) {
        categories = data.categories;
		genrate_categories(categories);
    });

	$.get("../resources/data/tasks.json", function(data) {
		tasks = data;
		genrate_tasks(101);
	});

	function genrate_categories(local_categories) {
		var list_Options = '';
        $.each(local_categories, function(index, element) {
			list_Options += '<li><a id='+ element.categoryId +' name="' + element.categoryName + '" href="#">' + element.categoryId + ' - ' + element.categoryName + ' (' + element.numberOfTasks + ')</a></li>';
        });
		$(".catagories").html(list_Options);
	}

	function genrate_tasks(categoryId) {
		selectedCategoryId = categoryId;
		var list_Options = '';
		$.each(tasks, function(index, element) {
			if(element[categoryId]) {
				$.each(element[categoryId], function(index, task) {	
					list_Options += '<tr>';
					list_Options += '<td><input type="checkbox" name="task[]"></td>';
					list_Options += '<td>'+ task.taskName +'</td>'
					list_Options += '<td>'+ task.deadline +'</td>';
					list_Options += '</tr>';
				});
			}
        });
		$(".tasks").html(list_Options);        
	}

	// Attach a delegated event handler
	$( ".catagories" ).on( "click", "a", function( event ) {
		event.preventDefault();
		$("#category-title").html( $( this ).attr("name") );
		genrate_tasks($( this ).attr("id"));
	});

	$( "#taskForm" ).submit(function( event ) {
		var newTask = {
			taskId :  Math.random(5),
			taskName : $("#taskName").val(),
			deadline: $("#deadlineDate").val()
		}
		tasks.tasks[selectedCategoryId].push(newTask);		
		genrate_tasks(selectedCategoryId);
        $('[data-popup="popup-1"]').fadeOut(350);
		event.preventDefault();
	});

    $('[data-popup-open]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        e.preventDefault();
    });

    $('[data-popup-close]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        e.preventDefault();
    });

    $("#searchText").keyup(function(){
		filter = new RegExp($(this).val(),'i');
		$("#filterme tbody tr").filter(function(){
			$(this).each(function(){
				found = false;
				$(this).children().each(function(){
					content = $(this).html();
					if(content.match(filter)) {
						found = true
					}
				});
				if(!found) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		});
	});
});

function removeSampleRow(id) {
    /***We get the table object based on given id ***/
    var objTable = document.getElementById(id);

    /*** Get the current row length ***/
    var iRow = objTable.rows.length;

	/*** Initial row counter ***/
	var counter = 0;

    /*** Performing a loop inside the table ***/
	if (objTable.rows.length > 1) {
		for (var i = 0; i < objTable.rows.length; i++) {

			 /*** Get checkbox object ***/
			var chk = objTable.rows[i].cells[0].childNodes[0];
			if (chk.checked) {
				/*** if checked we del ***/
				objTable.deleteRow(i);
				iRow--;
				i--;
				counter = counter + 1;
			}
		}

		/*** Alert user if there is now row is selected to be deleted ***/
		if (counter == 0) {
			alert("Please select the row that you want to delete.");
		}
	}else{
		/*** Alert user if there are no rows being added ***/
		alert("There are no rows being added");
	}
}
var defaultCategory = {
	name : "default",
	tasks: [{description  : "this is a default task", deadline : "date"}]
};




